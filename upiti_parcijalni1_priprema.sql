select * from Film
where Film.zanr = "komedija";

SELECT * FROM Glumac
where Glumac.god_rodjenja < 1993;

SELECT * FROM Glumac
JOIN Drzava on Drzava.id = Glumac.drzavljanin
WHERE (Glumac.pol = "muski" AND Drzava.naziv = "Amerika");

SELECT * FROM Film
Join Drzava on Drzava.id = Film.drzava_porekla
Where Film.godina > 2019 AND Drzava.naziv = "Srbija";

SELECT distinct Drzava.naziv, count(Film.id) AS Broj_Filmoa
FROM Film
join Drzava on Drzava.id = Film.drzava_porekla
Group By Drzava.id;

SELECT DISTINCT Film.naziv 
From Film
join film_glumac on film_glumac.film_id = Film.id
join Glumac on Glumac.id = film_glumac.glumac_id
join Drzava on Drzava.id = Film.drzava_porekla

